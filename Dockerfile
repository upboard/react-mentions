FROM node:20 as builder

# Copy source code and navigate to it.
COPY . /react-mentions
WORKDIR /react-mentions

# Authenticate with Google Artifact Registry and install dependencies.
RUN cp .npmrc /root/.npmrc
RUN npx google-artifactregistry-auth
RUN npm ci --legacy-peer-deps

# Build the module.
RUN npm run build

# Publish the module to Google Artifact Registry.
RUN npm publish
